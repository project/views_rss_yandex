# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

The module provides Yandex XML namespace and item elements for Views RSS.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/views_rss_yandex>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/views_rss_yandex>


## REQUIREMENTS

  * views_rss <https://www.drupal.org/project/views_rss>
  * views_rss_core


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Views and create new view for
       your RSS feed.
    3. The article on russian available here <https://drupal.ru/node/144941>


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
