<?php

namespace Drupal\views_rss_yandex\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views_rss\Plugin\views\style\RssFields;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default style plugin to render an RSS feed from fields.
 *
 * @ingroup views_style_plugins
 */
#[ViewsStyle(
  id: "rss_fields_yandex",
  title: new TranslatableMarkup("Yandex RSS Feed - Fields"),
  help: new TranslatableMarkup("Generates an RSS feed from fields in a view."),
  theme: "views_view_rss",
  display_types: ["feed"],
)]
class RssFieldsYandex extends RssFields {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['feed_settings']['contains']['yandex_turbo'] = ['default' => 0];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['feed_settings']['yandex_turbo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable yandex turbo for this feed'),
      '#default_value' => !empty($this->options['feed_settings']['yandex_turbo']),
      '#weight' => 0,
    ];
  }

}
